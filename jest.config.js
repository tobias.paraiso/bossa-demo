module.exports = {
	preset: 'ts-jest',
	testEnvironment: 'node',
	projects: ['<rootDir>/packages/**/jest.config.js'],
	cache: false,
	clearMocks: true,
	forceExit: true,
	resetMocks: true,
	restoreMocks: true,
	testMatch: ['*.test.ts', '*.test.tsx']
}
