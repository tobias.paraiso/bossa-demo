module.exports = {
	env: {
		browser: true,
		es2021: true,
		node: true,
		jest: true
	},
	extends: [
		'plugin:prettier/recommended',
		'prettier/prettier',
		'prettier',
		'plugin:react/recommended',
		'standard',
		'plugin:@typescript-eslint/recommended',
		'plugin:react/recommended'
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaFeatures: {
			jsx: true
		},
		ecmaVersion: 12,
		sourceType: 'module'
	},
	plugins: [
		'react',
		'@typescript-eslint',
		'eslint-plugin-import-helpers',
		'jest',
		'security'
	],
	settings: {
		react: {
			version: 'detect'
		},
		'import/parsers': {
			'@typescript-eslint/parser': ['.ts', '.tsx']
		},
		'import/resolver': {
			typescript: {}
		}
	},
	rules: {
		'prettier/prettier': 'error',
		indent: [
			'error',
			'tab',
			{ SwitchCase: 1, ignoredNodes: ['JSXElement'] }
		],
		camelcase: [2, { properties: 'always' }],
		'no-tabs': 'off',
		'no-use-before-define': 'off',
		'space-before-function-paren': 'off',
		'comma-dangle': [
			'error',
			{
				arrays: 'never',
				objects: 'never',
				imports: 'never',
				exports: 'never',
				functions: 'never'
			}
		],
		'import-helpers/order-imports': [
			'error',
			{
				newlinesBetween: 'always',
				groups: [
					'module',
					'/^#/',
					'/^@/',
					['parent', 'sibling', 'index']
				],
				alphabetize: { order: 'asc', ignoreCase: true }
			}
		]
	}
}
