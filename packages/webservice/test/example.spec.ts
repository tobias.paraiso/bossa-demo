import test from 'japa'
import supertest from 'supertest'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

test('ensure home page works', async () => {
	await supertest(BASE_URL).get('/').expect(403)
})
