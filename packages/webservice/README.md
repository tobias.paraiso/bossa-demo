# Getting Started

## Design

---

We use Domain driven design (DDD) approach to this software design that values simplicity and modeling code as closely to the business domain as possible. 

We isolate the domain code away from all other components of the system like infrastructure, security, etc;

The domain layer is the centre of the system. It contains domain objects, domain services and business rules.

## Installation

---

1. Install all dependencies of the project

```bash
npm install
# or
yarn
```

2. Copy env example file to your project

```bash
cp .env.example .env
```

3. Gerenate `APP_KEY` and paste to your `.env` file

```bash
node ace generate:key
```

4. Build docker image

teste3

```bash
docker compose up --build -d
```

Open [http://localhost:8080](http://localhost:8080) with your browser to see the result.


### Project tests

1. Run all tests from the project

```bash
yarn tests
```