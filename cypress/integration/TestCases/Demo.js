import { createArrayTypeNode } from "typescript";

describe('My First Test Suite', function()
{   
    it('Select radio button', function() {
        cy.visit('https://www.rahulshettyacademy.com/AutomationPractice/');
        cy.get('input.radioButton').eq(1).click()
        cy.wait(2000)
    })

    it('Select element in search field', function() {
        cy.visit('https://www.rahulshettyacademy.com/AutomationPractice/');
        cy.get('#autocomplete').type('ca')
        cy.wait(2000)
        cy.get('.ui-menu-item').each(($el, index, $list) => {
            const text_country = $el.find('.ui-menu-item-wrapper').text()
            if(text_country.includes('Antarctica'))
            {
                $el.click()
            }

        })
        //cy.get('#ui-id-3').should('have.length', 1)
        //cy.get('#ui-id-3').click()
        //cy.wait(1000)
        //cy.get('#autocomplete').contains('Antarctica')

    })

    it('Select dropdown element', function() {
        cy.visit('https://www.rahulshettyacademy.com/AutomationPractice/');
        cy.get('#dropdown-class-example').click()
        cy.wait(2000)
        
        //cy.get('#autocomplete').contains('Antarctica')

    })
})