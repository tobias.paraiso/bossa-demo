# hub.me

## Test E2E

Cypress folder
- TestCases
- Objects


Open cypress runner

`node_modules/.bin/cypress open`

`node_modules/.bin/cypress run`

`node_modules/.bin/cypress run --headed`

`node_modules/.bin/cypress run --browser firefox`

`node_modules/.bin/cypress run --browser chrome`

`npm run cy:run -- --record --spec "cypress/integration/TestCases/Demo.js"`

`npx run cy:run -- --record --spec "cypress/integration/TestCases/Demo.js"`