module.exports = {
	tabWidth: 4,
	semi: false,
	singleQuote: true,
	arrowParens: 'always',
	bracketSpacing: true,
	useTabs: true,
	endOfLine: 'auto',
	trailingComma: 'none'
}
